package com.socgen.training;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("jdbcdao")
public class JDBCDao {
	
	@Autowired
	DataSource dataSource;

	Connection dbCon;
	
	void connectionToDB() {
		try {
			dbCon=dataSource.getConnection();
			if(dbCon !=null) {
				System.out.println("Connected");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
