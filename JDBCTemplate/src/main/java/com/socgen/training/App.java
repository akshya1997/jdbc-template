package com.socgen.training;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("appContext.xml");
        
        JDBCDao dao=(JDBCDao) context.getBean("jdbcdao");
        
        dao.connectionToDB();
    }
}
